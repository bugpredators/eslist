import React, { StrictMode } from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { createTheme, makeStyles, ThemeProvider } from "@material-ui/core/styles";

import store from "./core/store";
import MainLayout from "./pages";

import ErrorBoundary from "./components/ErrorBoundary";
import { NativeEventSource, EventSourcePolyfill } from "event-source-polyfill";

const EventSource = NativeEventSource || EventSourcePolyfill;
global.EventSource = NativeEventSource || EventSourcePolyfill;



const palette = {

};

const muiTheme = createTheme({
  themeName: "My App Theme",
  palette: palette,
  margin: 4,
});

const RenderDom = props => {
  render(
    <Provider store={store}>
      <ThemeProvider theme={muiTheme}>
        <StrictMode>
          <ErrorBoundary>
            <Router>{IndexRoutes}</Router>
          </ErrorBoundary>
        </StrictMode>
      </ThemeProvider>
    </Provider>,
    document.getElementById("root")
  );
};

const IndexRoutes = (
  <Switch>
    <Route key="main" path="/" component={MainLayout} />
  </Switch>
);

RenderDom();
